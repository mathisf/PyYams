# -*- coding: utf-8 -*-
from ClassesPython3 import *
import random

"""
    Author : FAIVRE Mathis (mathis.faivre@outlook.com)
    Fichier python à éxécuter pour jouer au Yams (pour Python 2 et 3)
"""


cpu = False
pseudo1 = str(input('Pseudo du joueur 1 : '))
j1 = Joueur(pseudo1)

while True:
    choix2joueur = str(input('Jouer contre un autre joueur ? o/n : '))
    try:
       choix2joueur = str(choix2joueur)
    except ValueError:
       print('Reponse non valide')
       continue
    if choix2joueur == 'o' or choix2joueur == 'n':
       break
    else:
       print('Reponse non valide')

if choix2joueur == 'o' :
    pseudo2 = str(input('Pseudo du joueur 2 : '))
    j2 = Joueur(pseudo2)
    partie = Partie(j1, j2, cpu)
else :
    while True:
        choixcpu = str(input('Jouer contre un le cpu ? o/n : '))
        try:
           choixcpu = str(choixcpu)
        except ValueError:
           print('Reponse non valide')
           continue
        if choixcpu == 'o' or choixcpu == 'n':
           break
        else:
           print('Reponse non valide')
        if choixcpu == 'o' :
            cpu = True
    partie = Partie(j1, None, cpu)
    
partie.jouer()